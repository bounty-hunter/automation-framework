package com.demo.reporter;

import static com.demo.constants.CommonConstants.EXTENT_REPORT_HTML;

import java.io.IOException;

import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;


/**
 * This class is responsible for whole extent reporting operations.
 */
public class ExtentReporter {
	
	public static ExtentReports extentReports;
	public static ExtentTest test;


	 /** It will create and get instance of ExtentReports.
	 * @return instance of ExtentReports
	 */
	public static ExtentReports getInstance() {
	    	if (extentReports == null) {
	    		createInstance();
	    	}
	    	
	        return extentReports;
	    }
	    
	    /**
	     * @param fileName The filename which will be created for extent reporting
	     * @return instance of ExtentReports.
	     */
	    public static ExtentReports createInstance() {
	    	String reportName = "extent_report";
	        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(EXTENT_REPORT_HTML);

	    	htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
	        htmlReporter.config().setChartVisibilityOnOpen(true);
	        htmlReporter.config().setTheme(Theme.STANDARD);
	        htmlReporter.config().setDocumentTitle(reportName);
	        htmlReporter.config().setEncoding("utf-8");
	        htmlReporter.config().setReportName(reportName);

	        extentReports = new ExtentReports();
	        extentReports.attachReporter(htmlReporter);
	        
	        return extentReports;
	    }
	    
	    /**
	     * It will create instance of ExtentReports on starting of suite execution.
		 */
		public static void onExceutionStart() {
			extentReports = getInstance();
		}
	    
		/**
		 * @param result
		 */
		public static void onTestStart(ITestResult result) {
			extentReports = getInstance();
			test = extentReports.createTest(result.getMethod().getMethodName());
			test.assignAuthor(System.getProperty("user.name"));
			test.assignCategory("Smoke");
		}
		
		/**
		 * @param result
		 */
		public static void onTestPass(ITestResult result) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getMethod().getMethodName() + " :  PASSED", ExtentColor.GREEN));

		}
		
		/**
		 * @param result
		 */
		public static void onTestFail(ITestResult result) {
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getMethod().getMethodName() + " : FAILED", ExtentColor.RED));

		}
		
		/**
		 * @param result
		 */
		public static void onTestSkip(ITestResult result) {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getMethod().getMethodName() + " : SKIPPED", ExtentColor.ORANGE));

		}
		
		/**
		 * 
		 */
		public static void onSuiteFinish() {
			extentReports.flush();
		}

		
		/** It will add provided info on extent report on extent report.
		 * @param details
		 */
		public static void info(String details) {
			test.log(Status.INFO, MarkupHelper.createLabel(details, ExtentColor.WHITE));
		}
		
		/** It will add pass status on test step on extent report.
		 * @param details
		 */
		public static void pass(String details) {
			test.log(Status.PASS, MarkupHelper.createLabel(details, ExtentColor.GREEN));
		}
		
		/** It will add fail status on test step on extent report.
		 * @param details
		 */
		public static void fail(String details) {
			test.log(Status.FAIL, MarkupHelper.createLabel(details, ExtentColor.RED));
		}
		
		/**
		 * @param details Throwable object
		 */
		public static void fail(Throwable details) {
			test.log(Status.FAIL, details);
		}
		
		/** It will add warning status on test step on extent report.
		 * @param details
		 */
		public static void warning(String details) {
			test.log(Status.WARNING, MarkupHelper.createLabel(details, ExtentColor.YELLOW));
		}
		
	/** It will add attach screenshot to extent report.
	 * @param screenShotPath
	 */
	public static void attachScreenshot(String screenShotPath) {
		try {
			test.addScreenCaptureFromPath(screenShotPath);
		} catch (IOException e) {
			fail("!!!!!!!!! Exception occurred while attaching the screenshot to extent report.");
		}

	}
	
}
