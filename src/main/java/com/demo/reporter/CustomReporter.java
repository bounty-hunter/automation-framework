package com.demo.reporter;

import java.io.File;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IExecutionListener;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.xml.XmlSuite;

import com.demo.utils.common.FileUtil;

import static com.demo.constants.CommonConstants.*;

/**
 * It provides complete reporting of the suite execution.
 *
 */
public class CustomReporter extends TestListenerAdapter implements IReporter, IExecutionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomReporter.class);

	@Override
	public void onTestSkipped(ITestResult testResult) {
		ExtentReporter.onTestSkip(testResult);
		LOGGER.info(LOG_DESIGN + "Test Case Skipped: {}", testResult.getMethod().getMethodName());
		onTestCompletion(testResult, "SKIPPED");
	}

	@Override
	public void onTestStart(ITestResult testResult) {
		ExtentReporter.onTestStart(testResult);
		LOGGER.info(LOG_DESIGN + "Execution Started For Test: {}", testResult.getMethod().getMethodName());
	}

	@Override
	public void onTestSuccess(ITestResult testResult) {
		ExtentReporter.onTestPass(testResult);
		LOGGER.info(LOG_DESIGN + "Test Case Passed : {}", testResult.getMethod().getMethodName());
		onTestCompletion(testResult, "PASSED");

	}

	@Override
	public void onTestFailure(ITestResult testResult) {
		ExtentReporter.fail(testResult.getThrowable());
		ExtentReporter.onTestFail(testResult);
		LOGGER.error(LOG_DESIGN + "Test Case Failed: {}", testResult.getMethod().getMethodName());
		LOGGER.error(LOG_DESIGN + "Failure Reason : ", testResult.getThrowable());
		onTestCompletion(testResult, "FAILED");

	}

	@Override
	public void generateReport(List<XmlSuite> arg0, List<ISuite> arg1, String arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onExecutionFinish() {
		ExtentReporter.onSuiteFinish();
		LOGGER.info("                            ##########################                                 ");
		LOGGER.info("############################ SUITE EXECUTION FINISHED ################################");
		LOGGER.info("                            ##########################                                 ");

	}

	@Override
	public void onExecutionStart() {
		LOGGER.info("                            ##########################                                 ");
		LOGGER.info("############################ SUITE EXECUTION STARTED ################################");
		LOGGER.info("                            ##########################                                 ");
		ExtentReporter.onExceutionStart();
		
		FileUtil.createDirectory(TEST_RESULT_DIRECTORY_PATH);
		FileUtil.createDirectory(TEST_CASE_SCREENSHOTS_PATH);
		FileUtil.createDirectory(EXECUTION_REPORTS_PATH);

		LOGGER.info(LOG_DESIGN + "Test Result Directories created.");

	}
	

	/**
	 * It will be used to perform the after test steps if any.
	 * 
	 * @param testResult
	 */
	private void onTestCompletion(ITestResult testResult, String testStatus) {
		String testCaseName = testResult.getMethod().getMethodName();
		ITestContext context = testResult.getTestContext();
		WebDriver driver = (WebDriver) context.getAttribute(DRIVER);
		String screenShotPath = TEST_CASE_SCREENSHOTS_PATH + File.separatorChar + testCaseName + PNG_EXTENSION;
		ScreenShotGenerator.captureScreenshot(driver,
				TEST_CASE_SCREENSHOTS_PATH + File.separatorChar + testCaseName + PNG_EXTENSION);
		if (testStatus.equalsIgnoreCase("FAILED")) {
			ExtentReporter.attachScreenshot(screenShotPath);
		}
		LOGGER.info("######################################## TEST CASE {} ################################################", testStatus);
		LOGGER.info("");
	}
}
