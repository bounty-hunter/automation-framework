package com.demo.utils.selenium.mobile;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class AppiumChromeDemo {
	public static WebDriver driver;
	public static String appURL = "http://www.oracle.com";

	public static void main(String r[]) throws MalformedURLException {

		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability("browserName", "chrome");
		capabilities.setCapability("deviceName", "13");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appActivity", "com.google.android.apps.chrome.Main");

		driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		driver.navigate().to(appURL);
		System.out.println(driver.getTitle());
		driver.findElement(By.cssSelector(".u24visit div a")).click();
	}
}
