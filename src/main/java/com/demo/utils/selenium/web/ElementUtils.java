package com.demo.utils.selenium.web;

import static com.demo.constants.CommonConstants.LOG_DESIGN;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.reporter.ExtentReporter;
import com.demo.utils.common.Config;

/**
 * This class is responsible for performing all required user actions to
 * automate a web application. It is generally made for web applications that
 * run on desktop(e.g Windows/Mac etc.) browsers. Some methods might work for
 * mobile devices also but complete support is not present.
 *
 */
public class ElementUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(ElementUtils.class);
	public static WebDriver driver = null;

	
	public static void setDriver(WebDriver driver) {
		ElementUtils.driver = driver;
	}
	
	
	/**
	 * It will get element.
	 * 
	 * @param locator
	 * @throws Exception
	 */
	public static WebElement getElement(String locator) {
		String locatorValue = Config.getProperty(locator);
		LOGGER.info(LOG_DESIGN + "Getting element for :-- [{}] : [{}]", locator, locatorValue);
		ExtentReporter.info("Getting element for :-- [" + locator + "] : " + "[" + locatorValue + "]");
		WebElement element = null;
		try {
			element = driver.findElement(getByObject(locator));
		} catch (Exception e) {
			LOGGER.error(LOG_DESIGN + "!!!!!! Exception Occurred : {}: ", e.getMessage());
			ExtentReporter.fail("!!!!!! Exception Occurred : " + e.getMessage());
		}

		return element;
	}
	
	/** It will give the object of "By" : 
	 * @param locator
	 * @return By object
	 */
	public static By getByObject(String locator) {
		String locatorValue = Config.getProperty(locator);
		By byObj = null;
		if (locator.endsWith("_xpath")) {
			byObj = By.xpath(locatorValue);
		} else if (locator.endsWith("_css")) {
			byObj = By.cssSelector(locatorValue);
		} else if (locator.endsWith("_id")) {
			byObj = By.id(locatorValue);
		} else if (locator.endsWith("__linkText")) {
			byObj = By.linkText(locatorValue);
		}
		
		return byObj;
		
	}
	
	/**  It just check that an element is present on the DOM of a page. 
	 * @param locator
	 * @param seconds
	 */
	public static void waitForElementPresence(String locator, long seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.presenceOfElementLocated(getByObject(locator)));
	}
	
	/** It will check that an element is present on the DOM of a page and visible. 
	 * @param locator
	 * @param seconds
	 */
	public static void waitForElementVisibility(String locator, long seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getByObject(locator)));
	}
	
	/**
	 * It will click on a given locator.
	 * 
	 * @param locator
	 */
	public static void click(String locator) {
		try {
			LOGGER.info(LOG_DESIGN + "Clicking on : [{}] : [{}]", locator, Config.getProperty(locator));
			ExtentReporter.info("Clicking on : [" + locator + "]");
			WebElement element = getElement(locator);
			highlightWebElement(element);
			element.click();
		} catch (Exception e) {
			LOGGER.error(LOG_DESIGN + "Exception occurred while clicking : [{}]", e.getMessage());
			ExtentReporter.fail("Exception occurred while clicking on : [" + locator + "] :- " + e.getMessage());
		}
	}

	/** It will navigate to the specified URL.
	 * @param URL
	 */
	public static void navigateToURL(String URL) {
		ExtentReporter.info("Navigating to URL : " + URL);
		driver.navigate().to(URL);
		
	}
	
	/** It will give the page title.
	 * @return
	 */
	public static String getPageTitle() {
		String pageTitle  = driver.getTitle();
		ExtentReporter.info("Page Title is : " + driver.getTitle());
		
		return pageTitle;
		
	}
	
	/** It will get an element's text using any attribute.
	 * For e.g attribute can be : innerText,textContent,title etc as defined in the element html code
	 * @param locator
	 * @param attribute
	 * @return
	 */
	public static String getTextUsingAttribute(String locator, String attribute) {
		WebElement element = getElement(locator);
		highlightWebElement(element);
		String elementText = element.getAttribute(attribute);
		ExtentReporter.info("Text found for : [" + locator  + "] is : [" + elementText + "]");
		
		return elementText;
		
	}
	
	/** It enters the value in text box.
	 * @param locator
	 * @param text
	 */
	public static void enterText(String locator, String text) {
		WebElement element = getElement(locator);
		highlightWebElement(element);
		ExtentReporter.info("Entering text for : [" + locator + "] ::   Text is : [" + text + "]");
		element.sendKeys(text);
		
	}
	
    /**
     * It focuses on given web element.
     * 
     * @param driver
     * @param element
     */
    public static void jsFocus(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].focus();", element);
    }
    
	/**
	 * It scrolls to the given WebElement.
	 * 
	 * @param driver
	 * @param element
	 * @return WebElement
	 */
    public static WebElement scrollingToElementofAPage(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
       
        return element;
    }
    
    /** It will highlight the web element
     * @param element
     */
    public static void highlightWebElement(WebElement element) {
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].setAttribute('style', 'background:#ffffb3; border:3px solid green;');", element);
    }
    
	/**
	 * This method copies the content to system Clipboard and than paste it. i.e it
	 * just performs copy paste operation like (Ctrl C + Ctrl V)
	 * 
	 * @param element
	 * @param stringToBePasted
	 */
	public static void copyPaste(WebElement element, String stringToBePasted) {
		LOGGER.info(LOG_DESIGN+ "Clearing any text if present..");
		element.clear();
		element.click();
		LOGGER.info(LOG_DESIGN + "copy pasting : [{}] .", stringToBePasted);
		StringSelection stringSelection = new StringSelection(stringToBePasted);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		} catch (AWTException e) {
			LOGGER.error(LOG_DESIGN + "!!!!!!!! Exception occured while copy pasting the given content....: {}", e.getMessage());
		}
		
	}
}
