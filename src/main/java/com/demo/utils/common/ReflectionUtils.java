package com.demo.utils.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

public class ReflectionUtils {

	public static void main(String r[]) {
		String packagePath = "com.demo.core";
		Map<String, Map<String, String>> testAndClasses = getClassAndTests(packagePath);

		testAndClasses.entrySet().forEach(data -> {
			System.out.println(data.getKey());
			System.out.println(data.getValue());
			System.out.println("#################################");
		});

	}

	/**
	 * 
	 */
	public static Map<String, Map<String, String>> getClassAndTests(String packagePath) {
		Map<String, Map<String, String>> testNamesCache = new HashMap<>();
		List<Class<? extends Object>> classes = getAllClasses(packagePath);

		classes.forEach(klass -> {
			Map<String, String> testIdNamePair = new HashMap<>();
			String className = klass.getName();
			List<Method> methods = findAnnotatedMethods(klass, org.testng.annotations.Test.class);
			methods.forEach(method -> {
				String testCaseName = method.getName();
				String testCaseId = method.getAnnotation(org.testng.annotations.Test.class).testName();
				testIdNamePair.put(testCaseId, testCaseName);
			});
			testNamesCache.put(className, testIdNamePair);
		});
		
		return testNamesCache;
		
		
	}

	public static List<Method> findAnnotatedMethods(Class<?> clazz, Class<? extends Annotation> annotationClass) {
		Method[] methods = clazz.getMethods();
		List<Method> annotatedMethods = new ArrayList<Method>(methods.length);
		for (Method method : methods) {
			if (method.isAnnotationPresent(annotationClass)) {
				annotatedMethods.add(method);
			}
		}

		return annotatedMethods;
	}

	public static List<Class<? extends Object>> getAllClasses(String packagePath) {
		List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());

		Reflections reflections = new Reflections(new ConfigurationBuilder()
				.setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
				.setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
				.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(packagePath))));

		return reflections.getSubTypesOf(Object.class).stream().collect(Collectors.toList());

	}
}
