package com.demo.utils.common;

import org.testng.Assert;

import com.demo.reporter.ExtentReporter;

public class CustomAssertion {
	
	/** It will make assertion of two strings.
	 * @param actual
	 * @param expected
	 */
	public static void assertEquals(String actual, String expected) {
		ExtentReporter.info("Validating :- Actual [" + actual + "] : " + "Expected [" + expected + "]");
		Assert.assertEquals(actual, expected);
		
	}

}
