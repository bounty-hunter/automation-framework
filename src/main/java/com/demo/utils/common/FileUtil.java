package com.demo.utils.common;

import static com.demo.constants.CommonConstants.LOG_DESIGN;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * It contains utility methods for performing file related operations (e.g
 * create,read etc.).
 *
 */
public class FileUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

	/**
	 * It creates directory or folder at provided path
	 * 
	 * @param directoryPath
	 */
	public static void createDirectory(String directoryPath) {
		LOGGER.info(LOG_DESIGN + "Creating directory : {}", directoryPath);
		(new File(directoryPath)).mkdirs();
	}

}
