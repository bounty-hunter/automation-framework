package com.demo.core;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.demo.modal.Account;
import com.demo.modal.CommonData;
import com.demo.modal.InputTestData;

public class TestUtils {

	/** It will return complete data set present in data xls file.
	 * @param tcId : TetsCase id
	 * @return InputTestData object
	 */
	public static InputTestData getCompleteTestData(String tcId) {
        return TCDataRecorder.getInputTestData();
    }
	
	/** It will get the data present in "TESTDATA" sheet in data xls file.
	 * @param tcId TetsCase id
	 * @return CommonData object
	 */
	public static CommonData getTestData(String tcId) {
        return TCDataRecorder.getInputTestData().getCommonData().get(TCDataRecorder.TC_MAPPING.get(tcId).getDataId());
    }
	
	/**
	 * @param accountDataId : id present in first column of "ACCOUNT" sheet in data xls file.
	 * @return Account object
	 */
	public static Account getAccountData(String accountDataId) {
        return TCDataRecorder.getInputTestData().getAccountSheetData().get(accountDataId);
    }
	
	/** It will rerun list of all Account data id
	 * e.g [ACC_DATA_1,ACC_DATA_2,ACC_DATA_3]
	 * @return
	 */
	public static List<String> getAccountSheetIds() {
		return TCDataRecorder.getInputTestData().getAccountSheetData().keySet().stream().collect(Collectors.toList());
	}
	
	/** It will convert list of id into a 2D array required for data provider.
	 * @param dataIds
	 * @return
	 */
	public static Object[][] getSheetData(List<String> dataIds) {
		int totalDataIds = dataIds.size();
		Object[][] result = new Object[totalDataIds][1];
		IntStream.range(0, totalDataIds).forEach(index -> {
			result[index][0] = dataIds.get(index);
			System.out.println(result[index][0]);
		});

		return result;

	}
	
	
}
