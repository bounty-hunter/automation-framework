package com.demo.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.testng.ITestNGListener;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.demo.modal.TestScriptConfig;
import com.demo.reporter.CustomReporter;
import com.demo.utils.common.ReflectionUtils;

public class SuiteGenerator {

	public static Map<String, Map<String, String>> testAndClasses = new HashMap<>();
	
	public static void main(String r[]) {
		String packagePath = "com.demo.test";
		String dataFilePath = System.getProperty("user.dir") + "\\src\\main\\resources\\config\\demo_data.xlsx";
		testAndClasses = ReflectionUtils.getClassAndTests(packagePath);
		TCDataRecorder.record(dataFilePath);
		
		genrateTestNGANdRun();
	}
	
	public static void genrateTestNGANdRun() {
		XmlSuite suite = new XmlSuite();
		suite.setName("SmokeSuite");
		suite.setVerbose(8);

		List<String> tests = testAndClasses.keySet().stream().collect(Collectors.toList());
		
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		
		tests.forEach(testName -> {
			List<XmlInclude> includedMethods = new ArrayList<>();
			List<String> testIdsPresentInClass = testAndClasses.get(testName).keySet().stream().collect(Collectors.toList());
			List<String> filteredTests = testIdsPresentInClass.stream().filter(id -> TCDataRecorder.TC_MAPPING.get(id).getRunMode().equals("Y")).collect(Collectors.toList());
			filteredTests.forEach(mapper -> includedMethods.add(new XmlInclude(testAndClasses.get(testName).get(mapper))));
		//	List<String> excludedMethods = testAndClasses.get(testName).entrySet().stream().map(testdata -> testdata.getValue()).collect(Collectors.toList());
			
			String className = testName.split("\\.")[testName.split("\\.").length -1 ];
			TestScriptConfig testScriptConfig =  TCDataRecorder.TESTSCRIPT_CONFIG_CACHE.get(className);
			
			if (testScriptConfig.getRunMode().equalsIgnoreCase("Y")) {
			List<XmlClass> classes = new ArrayList<XmlClass>();
			XmlTest test = new XmlTest(suite);
			test.setName(className);
			
			Map<String, String> parameters = new HashMap<>();
			parameters.put("browser", testScriptConfig.getBrowser());
			parameters.put("nodeURL", testScriptConfig.getNodeURL());
			test.setParameters(parameters);
			XmlClass xmlClass = new XmlClass(testName);
			xmlClass.setIncludedMethods(includedMethods);
			//xmlClass.setExcludedMethods(excludedMethods);
				classes.add(xmlClass);
				test.setXmlClasses(classes);
			}
			
			
		});
		suites.add(suite);
		System.out.println(suite.toXml());
		TestNG tng = new TestNG();
		List<Class<? extends ITestNGListener>> listenerClasses = Arrays.asList(CustomReporter.class);
		tng.setXmlSuites(suites);
		tng.setListenerClasses(listenerClasses);
		tng.run();
	
	}	
	
}
