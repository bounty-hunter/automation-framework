package com.demo.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.modal.Account;
import com.demo.modal.CommonData;
import com.demo.modal.InputTestData;
import com.demo.modal.TCMapping;
import com.demo.modal.TestScriptConfig;

public class TCDataRecorder {

	private static final Logger LOGGER = LoggerFactory.getLogger(TCDataRecorder.class);
	public static InputTestData inputTestData;
	public static final Map<String, TestScriptConfig> TESTSCRIPT_CONFIG_CACHE = new LinkedHashMap<>();
	public static final Map<String, TCMapping> TC_MAPPING = new LinkedHashMap<>();
	public static final Map<String, CommonData> TC_COMMON_DATA_CACHE = new LinkedHashMap<>();
	public static final Map<String, Account> ACCOUNT_DATA_CACHE = new LinkedHashMap<>();

	/** It will record all the test data present in data xls file.
	 * @param inputFilePath : path where excel file is present
	 * e.g D:\\ARENA\\demo-framework\\src\\main\\resources\\config\\demo_data.xlsx
	 */
	public static void record(String inputFilePath) {
		File inputFile = new File(inputFilePath);
		FileInputStream fileInputStream;
		XSSFWorkbook workbook = null;
		try {
			fileInputStream = new FileInputStream(inputFile);
			workbook = new XSSFWorkbook(fileInputStream);
			recordTestScriptsConfig(workbook);
			recordTCMapping(workbook);
			recordTestData(workbook);
			recordAccountSheetData(workbook);
			inputTestData = new InputTestData(TC_COMMON_DATA_CACHE, ACCOUNT_DATA_CACHE);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}

	private static void recordTestScriptsConfig(XSSFWorkbook workbook) throws IOException {
		XSSFSheet sheet = workbook.getSheet("TestScripts");
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				String className = row.getCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String runMode = row.getCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String browser = row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String nodeURL = row.getCell(3, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				TestScriptConfig config = new TestScriptConfig(className, runMode, browser, nodeURL);

				TESTSCRIPT_CONFIG_CACHE.put(className, config);
			}
		}
	}

	private static void recordTCMapping(XSSFWorkbook workbook) throws IOException {
		XSSFSheet sheet = workbook.getSheet("TC_MAPPING");
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				String tcId = row.getCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String runMode = row.getCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String dataId = row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				if (dataId.isEmpty()) {
					dataId = "DEFAULT_DATA";
				}
				String dataSet = row.getCell(3, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				TCMapping tcMapping = new TCMapping(tcId, runMode, dataId, dataSet);

				TC_MAPPING.put(tcId, tcMapping);
			}
		}
	}
	
	private static void recordTestData(XSSFWorkbook workbook) throws IOException {
		XSSFSheet sheet = workbook.getSheet("TESTDATA");
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				String dataId = row.getCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String userName = row.getCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String password = row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				int accountId = (int)row.getCell(3, MissingCellPolicy.CREATE_NULL_AS_BLANK).getNumericCellValue();
				CommonData inputTestData = new CommonData(dataId, userName, password, accountId);

				TC_COMMON_DATA_CACHE.put(dataId, inputTestData);
			}
		}
	}
	
	private static void recordAccountSheetData(XSSFWorkbook workbook) throws IOException {
		XSSFSheet sheet = workbook.getSheet("ACCOUNTS");
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				String id = row.getCell(0, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String runMode = row.getCell(1, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				int accountId = (int)row.getCell(2, MissingCellPolicy.CREATE_NULL_AS_BLANK).getNumericCellValue();
				String accountName = row.getCell(3, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				String accountType = row.getCell(4, MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
				Account account = new Account(id, runMode, accountId, accountName, accountType);
				if (runMode.equalsIgnoreCase("Y")) {
				ACCOUNT_DATA_CACHE.put(id, account);
				}
			}
		}
	}

	public static InputTestData getInputTestData() {
		return inputTestData;
		
	}
	
	public static Object[][] readData(List<String> dataIds) {
		int totalDataIds = dataIds.size();
		Object[][] result = new Object[totalDataIds][1];
		IntStream.range(0, totalDataIds).forEach(index -> {
			result[index][0] = dataIds.get(index);
			System.out.println(result[index][0]);
		});

		return result;

	}
	
}
