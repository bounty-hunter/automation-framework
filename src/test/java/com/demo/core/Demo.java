package com.demo.core;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Demo {

	public static Object[][] result;
	public static void main(String[] args) {
		
		/*TCDataRecorder.record("D:\\ARENA\\demo-framework\\src\\main\\resources\\config\\demo_data.xlsx");
		List<String> ids = Arrays.asList("ID1","ID2", "ID3");
		Object[][] result = new Object[ids.size()][];
		Arrays.stream(result).forEach(a -> Arrays.fill(a, "hello"));
		System.out.println(result);*/
		List<String> ids = Arrays.asList("ID1", "ID2", "ID3");
	
		Object[][] data = readData(ids);
		for (int i = 0; i<data.length; i++) {
			System.out.println(data[i][0]);
		}

	}
	
	
	public static Object[][] readData(List<String> dataIds) {
		int totalDataIds = dataIds.size();
		Object[][] result = new Object[totalDataIds][1];
		IntStream.range(0, totalDataIds).forEach(index -> {
			result[index][0] = dataIds.get(index);
		});

		return result;

	}
	

    @DataProvider(name = "TestData")
    public Object[][] TestData(ITestContext context) {
    	List<String> ids = Arrays.asList("ID1", "ID2", "ID3");
       
    	return readData(ids);
    }
	
	@Test(dataProvider = "TestData")
	public static void demoo(String accountDataId) {
		System.out.println(accountDataId);
		
	}
}
