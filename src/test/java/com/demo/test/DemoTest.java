package com.demo.test;

import static com.demo.constants.CommonConstants.INNER_TEXT;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.demo.core.TestUtils;
import com.demo.core.WebUtils;
import com.demo.utils.common.Config;
import com.demo.utils.common.CustomAssertion;
import com.demo.utils.selenium.driver.DriverManager;
import com.demo.utils.selenium.driver.DriverPool;

public class DemoTest {

    private static WebDriver driver;
    private  ITestContext context;
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoTest.class);

    @Parameters({"browser","nodeURL"})
    @BeforeClass
	public void setup(String browser, String nodeURL, ITestContext ctx) {
		try {
			driver = DriverPool.getDriver(browser, nodeURL);
			WebUtils.setDriver(driver);
			driver.manage().window().maximize();
		} catch (Exception e) {
			LOGGER.error("Error in driver instantiation {} ", e.getMessage());
			throw new WebDriverException(e.getMessage());
		}
		
		this.context = DriverManager.setupContext(driver, ctx, browser, nodeURL);
	}

    @DataProvider(name = "AccountSheetData")
    public Object[][] TestData(ITestContext context) {
    	return TestUtils.getSheetData(TestUtils.getAccountSheetIds());
    }
    
    @Test (testName = "TEST_1")
    public void demo_test1() {
			System.out.println(TestUtils.getTestData("TEST_1"));
			System.out.println("Account data 1 is :" + TestUtils.getAccountData("ACC_DATA_1"));
    		/* WebUtils.navigateToURL(Config.getProperty("DishURL"));
    		 WebUtils.click("new_connection_link_xpath");
    		// String expectedProduct = WebUtils.getTextUsingAttribute("connection_description_id", INNER_TEXT).replaceAll("^.|.$", "");
    		 String expectedProduct = "DishNXT HD + DISHSMRT Stick";
    		 WebUtils.click("book_button1_id");
    		 WebUtils.enterText("pincode_txt_box_id", "201303");
    		 WebUtils.click("pincode_submit_button_id");
    		 WebUtils.waitForElementVisibility("selected_product_desc_id", 60);
    		 String actualSelectedProduct = WebUtils.getTextUsingAttribute("selected_product_desc_id", INNER_TEXT);
    		 CustomAssertion.assertEquals(actualSelectedProduct, expectedProduct);*/
    }
	
	
    @Test (testName = "TEST_2", dataProvider = "AccountSheetData")
    public void demo_test2(String accDataId) {
    	System.out.println("Browser is : " + this.context.getAttribute("browser"));
		System.out.println("TestData is :" + TestUtils.getTestData("TEST_2"));
		System.out.println("Account Id is :" + accDataId);
		System.out.println("Account data is :" + TestUtils.getAccountData(accDataId));
		System.out.println("###################################################################################");
		/* WebUtils.navigateToURL(Config.getProperty("DishURL"));
		 WebUtils.click("new_connection_link_xpath");
		 String expectedProduct = WebUtils.getTextUsingAttribute("connection_description_id", INNER_TEXT);
		 WebUtils.click("book_button1_id");
		 WebUtils.enterText("pincode_txt_box_id", "201303");
		 WebUtils.click("pincode_submit_button_id");
		 WebUtils.waitForElementVisibility("selected_product_desc_id", 60);
		 String actualSelectedProduct = WebUtils.getTextUsingAttribute("selected_product_desc_id", INNER_TEXT);
		 CustomAssertion.assertEquals(actualSelectedProduct, expectedProduct);*/
    }
    

   // @AfterClass
    public void tearDown() {
         driver.close();
         driver.quit();
    }
}
