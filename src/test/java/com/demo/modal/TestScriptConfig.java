package com.demo.modal;

public class TestScriptConfig {

	private String className;
	private String runMode;
	private String browser;
	private String nodeURL;

	public TestScriptConfig(String className, String runMode, String browser, String nodeURL) {
		this.className = className;
		this.runMode = runMode;
		this.browser = browser;
		this.nodeURL = nodeURL;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getRunMode() {
		return runMode;
	}

	public void setRunMode(String runMode) {
		this.runMode = runMode;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getNodeURL() {
		return nodeURL;
	}

	public void setNodeURL(String nodeURL) {
		this.nodeURL = nodeURL;
	}

	@Override
	public String toString() {
		return "TestScriptConfig [className=" + className + ", runMode=" + runMode + ", browser=" + browser
				+ ", nodeURL=" + nodeURL + "]";
	}

}
