package com.demo.modal;

public class Account {

	private String id;
	private String runMode;
	private int accountId;
	private String accountName;
	private String accountType;

	public Account(String id, String runMode, int accountId, String accountName, String accountType) {
		this.id = id;
		this.runMode = runMode;
		this.accountId = accountId;
		this.accountName = accountName;
		this.accountType = accountType;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRunMode() {
		return runMode;
	}

	public void setRunMode(String runMode) {
		this.runMode = runMode;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", runMode=" + runMode + ", accountId=" + accountId + ", accountName="
				+ accountName + ", accountType=" + accountType + "]";
	}

}
