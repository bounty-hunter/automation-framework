package com.demo.modal;

public class TCMapping {

	private String tcId;
	private String runMode;
	private String dataId;
	private String dataSet;

	public TCMapping(String tcId, String runMode, String dataId, String dataSet) {
		this.tcId = tcId;
		this.runMode = runMode;
		this.dataId = dataId;
		this.dataSet = dataSet;
	}

	public String getTcId() {
		return tcId;
	}

	public void setTcId(String tcId) {
		this.tcId = tcId;
	}

	public String getRunMode() {
		return runMode;
	}

	public void setRunMode(String runMode) {
		this.runMode = runMode;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String getDataSet() {
		return dataSet;
	}

	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@Override
	public String toString() {
		return "TCMapping [tcId=" + tcId + ", runMode=" + runMode + ", dataId=" + dataId + ", dataSet=" + dataSet + "]";
	}

}
