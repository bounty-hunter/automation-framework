package com.demo.modal;

public class CommonData {

	private String dataId;
	private String userName;
	private String password;
	private int accountId;

	public CommonData(String dataId, String userName, String password, int accountId) {
		this.dataId = dataId;
		this.userName = userName;
		this.password = password;
		this.accountId = accountId;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	@Override
	public String toString() {
		return "CommonData [dataId=" + dataId + ", userName=" + userName + ", password=" + password + ", accountId="
				+ accountId + "]";
	}
}
