package com.demo.modal;

import java.util.Map;

public class InputTestData {

	private Map<String, CommonData> commonData;
	private Map<String, Account> accountSheetData;
	
	public InputTestData (Map<String, CommonData> commonData, Map<String, Account> accountSheetData) {
		this.commonData = commonData;
		this.accountSheetData = accountSheetData;
	}

	public Map<String, CommonData> getCommonData() {
		return commonData;
	}

	public void setCommonData(Map<String, CommonData> commonData) {
		this.commonData = commonData;
	}

	public Map<String, Account> getAccountSheetData() {
		return accountSheetData;
	}

	public void setAccountSheetData(Map<String, Account> accountSheetData) {
		this.accountSheetData = accountSheetData;
	}

	@Override
	public String toString() {
		return "InputTestData [commonData=" + commonData + ", accountSheetData=" + accountSheetData + "]";
	}

}
